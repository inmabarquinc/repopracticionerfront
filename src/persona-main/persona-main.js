import { LitElement, html, css } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-main-dm/persona-main-dm.js'

class PersonaMain extends LitElement {
    
    /*
    //se va a devolver css con el filtrado
    //:host seleccoina todo el componente en stilos
    // all--> todos los estilos que tenga ponlo en incial.
    static get styles(){
        //esto lo que hace que 
        return css`
        :host{
            all: initial
        }
        `;
        } 
     */

    static get properties(){
        return{
            //se añade el array con lso datos de entrada
            //lo añadimos como propiedad
            people: {type: Array},
            showPersonForm: {type: Boolean}

        };
    }

    constructor(){
        super();

    
        this.people =[];
        //inicializamos el booleano qque vamos a utilizar para ocultar el person-main para mostrar el perso-form
        //por defecto ponemos false
         this.showPersonForm = false;
    }

    render (){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2> 
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado 
                            fname="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}" 
                        ></persona-ficha-listado>`
                    )}
                </div>
            </div>   
            <div class= "row">
                <persona-form 
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}" 
                    class="d-none border rounded border-primary"
                    id="personForm"> 
                </persona-form>
            </div>
            <div class= "row">
                <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
            </div>
    `; 
    }

   
    personFormStore(e){
        console.log("personFormStore");
        console.log("personFormStore Se ha cerrado e formulario de la persona");
       
        

        //tenemos que distinguir si estamos añadiendo o actualizando
        if(e.detail.editingPerson === true){
            console.log("se va actualizar la persona de nombre: "+e.detail.person.name)

            this.people=  this.people.map(
                person => person.name === e.detail.person.name 
                         ? person = e.detail.person : person);
                //si se cumple la condicion --> person.name === e.detail.person.name 
                //si es true se hace e.detail.person
                //si es false es person , no se modifica. Se deja asi.
        }else{
    
            console.log();

            // js spread syntax
            this.people= [...this.people, e.detail.person];
           // this.people= [this.people, e.detail.person];
        }



            //findIndex -- asi no nos devuelve un objeto, si no sería find
 /* let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name    
            );
            //si no lo encuentra devuelve -1 --- indexOfPerson >= 0 lo ha encontrado
            if(indexOfPerson >= 0){
                    this.people[indexOfPerson] = e.detail.person;
            }

        }else{
            //add
            console.log("Se va almacenar una persona nueva");
            this.people.push(e.detail.person);            
        }
        
  */
        //si concatenas object con string no sale bien en la console
        console.log(e.detail.person);
        this.showPersonForm = false;

    }
    
    updated(changedProperties){
        //ciclo de vida del litelement se ejecuta cuando cambia de valor de una propiedad del componente
   
        console.log("entramos a updated - persona-main");
        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();

            }
        }

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            //todo throw event
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {   detail: {
                        people: this.people
                    }
                }
                )
            )

                  
        }
    
    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        
        this.showPersonForm = false;

    }

    showPersonFormData(){
        console.log("Entro en showPersonFormData");
        console.log("Mostrar formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList(){
        console.log("Entro en showPersonList");
        console.log("Mostrar listado de persona");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }


    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log(e);
        console.log("Se va a borrar la personas de nombre" + e.detail.name);
            //this.people es el array
            //filter recibe como parámetro esa funcion 
            //cuando sea true el filter pasa, y cuando eso es false no pasa
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );

        /*
        shouldFilterPerson(person){
            return person.name != e.detail.name;
            //devuelve un boolean
        }

        */     
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log(e);
        console.log("Se ha pedido mas información de la persona" + e.detail.name);
            //this.people es el array
         
       //para buscar la persona
       //let parecido a var
       let chosenPerson=this.people.filter(
           person=> person.name===e.detail.name
       );
       console.log(chosenPerson);
       //CUIDADO SI HACEMOS el siguiente CONSOLE LOG, ya que sale undefined,  mejor hacer  console.log(chosenPerson); para ver el formato
       console.log(chosenPerson.name);
       //si queremos acceder al name seria asi:
       //console.log(chosenPerson[0].name);

       let person={}
       person.name=chosenPerson[0].name;
       person.profile=chosenPerson[0].profile;
       person.yearsInCompany=chosenPerson[0].yearsInCompany;

        console.log(chosenPerson[0].name);
        console.log(chosenPerson[0].profile);
        console.log(" yearsInCompany:"+ chosenPerson[0].yearsInCompany);

        //pasamos los valores al formulario

        this.shadowRoot.getElementById("personForm").person=person;
//para q aparezca deshabilitado estamos editando.
        this.shadowRoot.getElementById("personForm").editingPerson=true;
        
        this.showPersonForm= true;

    }


    peopleDataUpdated(e){
        console.log(" peopleDataUpdated");
        this.people = e.detail.people;
 
    }


}

customElements.define('persona-main', PersonaMain); 