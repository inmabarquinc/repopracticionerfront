import { LitElement, html } from "lit-element";
import '../emisor-evento/emisor-evento.js'
import '../receptor-evento/receptor-evento.js'

class GestorEvento extends LitElement {
    
    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render (){
        return html`
        <h1>Gestor evento</h1> 
        <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
        <receptor-evento id="receiver"></receptor-evento>
    `; 
    }

    processEvent(e){
        console.log("capturado evento del emisor");
        console.log(e);
       // console.log(e.detail);

        //código js seleccionar html?? 
        //lo hacemos con el DOM

        //shadowRoot es como el document pero para poder coger el valor de html
        //selector en base a un ID -- getElementById("receiver").
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;


    }
}

customElements.define('gestor-evento', GestorEvento);