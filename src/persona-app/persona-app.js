import { LitElement, html } from "lit-element";
import '../persona-header/persona-header.js'
import '../persona-main/persona-main.js'
import '../persona-footer/persona-footer.js'
import '../persona-sidebar/persona-sidebar.js'
import '../persona-stats/persona-stats.js'

class PersonaApp extends LitElement {
    
    static get properties(){
        return{
            people: {type : Array}

        };
    }

    constructor(){
        super();
    }
    
    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
            <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
            <persona-main @updated-people ="${this.updatePeople}"class="col-10"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
    `; 
    }


    updated(changedProperties){
        if (changedProperties.has("people")){
            console.log("updatePeople en persona-app");    
            console.log("ha cambiado el valor de la people en persona -app");   
            
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    updatePeople(e){
        console.log("updatePeople en persona-app");    

        //pasamos de persona app a persona stats.
        this.people=e.detail.people;

    }

    updatedPeopleStats(e){
        console.log("updatedPeopleStats en persona-app");   
        console.log(e.detail); 
        console.log(e.detail.peopleStats);   

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;

    }

    newPerson(e){
        console.log("newPerson en persona-app");    
        //cambiamos la propiedad de un componente
        //en este caso la propiedad showPersonForm de "persona-main"
        this.shadowRoot.querySelector("persona-main").showPersonForm= true;    
        //como persona-main no teien id utilizamos 'querySelector'
    }
}

customElements.define('persona-app', PersonaApp);