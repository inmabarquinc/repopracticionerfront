import { LitElement, html } from "lit-element";

class PersonaForm extends LitElement {
    
     
    static get properties(){
        return{
            //vamos a guardar toda la info que se incluya en el formualrio en in object
            person: {type: Object},
            //booleamo para manejar si estamos  añadiendo persona o actualziando
            editingPerson: {tyoe: Boolean}

        };
    }

    constructor(){
        super();
        //inicializamos 
       // this.person={};

        //Para q en la carga no aparezca el valor undefined, debemos de poner valores por defecto
       //comentamos el //this.person={}; xq lo hemos incluido en esta nueva funcion 
        this.resetFormData();

    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre completo</label>
                    <input  @input="${this.updateName}"   
                            type="text" 
                            class="form-control" 
                            placeholder= "Nombre completo"
                            .value="${this.person.name}"
                            ?disabled="${this.editingPerson}"
                    </input>
                </div>

                <div class="form-group">
                    <label>Perfil</label>
                    <textarea @input ="${this.updateProfile}" 
                                .value="${this.person.profile}"
                                class="form-control" 
                                placeholder= "Perfil"   
                                rows="5">
                    </textarea>
                </div> 
                
                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input @input="${this.updateYearsInCompany}" 
                            type="text" 
                            class="form-control" 
                            placeholder= "Años en la empresa"
                            .value="${this.person.yearsInCompany}"
                    </input>
                </div>
                <button @click="${this.goBack}"  class="btn btn-primary"><strong>Atrás</strong></button>
                <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>


            </form>
        </div>

    `; 
    }

    storePerson(e){
        console.log("storePerson"); 
        e.preventDefault();
        console.log("La propiedad name vale "+ this.person.name);
        console.log("La propiedad profile vale "+this.person.profile);
        console.log("La propiedad yearsInCompany vale "+this.person.yearsInCompany);
        this.person.photo ={
                src: "./img/persona.jpg",
                alt: "persona"
        }

     //   
        this.dispatchEvent(
            new CustomEvent(
                    "persona-form-store",
            {
            detail:{
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson: this.editingPerson
                }
            }

            )  
        )

        this.resetFormData();
    }

    updateProfile(e){
       console.log("actualizando el valor updateNameupdateProfile"); 
      // console.log("actualizando el valor name con el valor " + e.target.value);
       this.person.profile= e.target.value;
    }

    updateName(e){
       console.log("actualizando el valor updateName"); 
     //  console.log("actualizando el valor name con el valor " + e.target.value);

       this.person.name= e.target.value;

    }
    updateYearsInCompany(e){

        this.person.yearsInCompany= e.target.value;
    }

    goBack(e){
        //cómo hacer que no se haga el submit
        console.log("entramos en goBack");
        e.preventDefault();

        //se deben inicializar los values de los text area para q no quede el valor que hemos consultado
        this.resetFormData();

        this.dispatchEvent(new CustomEvent("persona-form-close", {}));

    }

    resetFormData(){
        console.log("Entramos en resetFormData() desde persona-form");

        //reinicializamos el objeto
        this.person={}
        this.person.name= "";
        this.person.profile= "";
        this.person.yearsInCompany= "";
         //reseteamos la variable para que vuelva a false
         //no estamos editando, y por lo tanto lo resetamos
        this.editingPerson =false;
    }
}

customElements.define('persona-form', PersonaForm);