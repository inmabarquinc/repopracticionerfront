import { LitElement, html } from "lit-element";


class FichaPersona extends LitElement {
    
    //para que litElement gestione la propiedad se debe de declara la propiedad en este primer bloque
    //devuelve un OBJECT que se define el nombre de propiedad y se le pueden pasar el nombre y unos parámetros de configuracion
    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    constructor(){
        super();
        
        this.name= "Prueba nombre";
        this.yearsInCompany = 12;

        //person info es una propiedad calculada
        /*
        if(this.yearsInCompany >=7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >=5){
            this.personInfo = "senior";
        }else if(this.yearsInCompany >=3){
            this.personInfo = "team";
        }else{
            this.personInfo = "junior";
        }
        */

        //no haria falta xq está  updated(changedProperties)
        this.updatePersonInfo();

    }

    /*ciclo de vida
    se llama cuando se ha cambiado el valor
    le llegan los valores que han cambiado de valor*/
    updated(changedProperties){
        console.log("dentro del updated")
        //prueba(oldValue, propName){
            //stuff
        //}
        changedProperties.forEach((oldValue, propName) => {
            console.log("la propiedad "+ propName+ " cambia de valor, anterior era "+ oldValue)
        })
       // this.name //es el valor nuevo
       //mirar si ha cambiado el valor 
       if(changedProperties.has("name")){
           console.log("cambia valor - el anterior era "
           + changedProperties.get("name") + " y nuevo es " + this.name);
       }
    //preguntamos si la popiedad 'name' ha cambiado 
    //si ha cambiado podemos utilziar el changedProperties.get("yearsInCompany") a coger el valor
    //xq ya sabemos q esa propiedad ha cambiado.
       if(changedProperties.has("yearsInCompany")){
        console.log("cambia valor - el anterior era "
        + changedProperties.get("yearsInCompany") + " y nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    /*bloque vista que se pinta*/
    render(){
        return html`
            <div>
                <label>Nombre completo</label>
                <input type= "text" id="fname" value="${this.name}" @change="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type= "text" id="fanosempresa" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}""></input>

                <input type= "text" value="${this.personInfo}"  disabled </input>

                <br />
                <button>Enviar</button>
            </div> 
        `; 
    }
    updateName(e){
        console.log("se ejecuta la funion updateName");
        //Cambiar el valor de la propiedad
        //target es el sitio donde se ha desencadenado el evento
        this.name = e.target.value;
        console.log("this.name=" + this.name);
    }

    updateYearsInCompany(e){
      //   <input type= "text" id="fanosempresa" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}""></input> 
        console.log("se ejecuta la funion updateYearsInCompany");
        //Cambiar el valor de la propiedad
        //target es el sitio donde se ha desencadenado el evento
        //que se ejecute el update del inicio --  updated(changedProperties){
        this.yearsInCompany = e.target.value;
      //  console.log("this.yearsInCompany=" + this.yearsInCompany);

       this.updatePersonInfo();

    }



    updatePersonInfo(){
        // <input type= "text" id="fanosempresa" value="${this.yearsInCompany}" @change="${this.loadPersonInfo}""></input>
        console.log("Entro en updatePersonInfo----------------");

        console.log("valor de  this.yearsInCompany----------------"+this.yearsInCompany);
        if(this.yearsInCompany >=7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >=5){
            this.personInfo = "senior";
        }else if(this.yearsInCompany >=3){
            this.personInfo = "team";
        }else{
            this.personInfo = "junior";
        }
    }

   


}

customElements.define('ficha-persona', FichaPersona);