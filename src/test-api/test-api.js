import { LitElement, html } from "lit-element";

class TestApi extends LitElement {
    
     
    static get properties(){
        return{
                movies: {type: Array}
        };
    }

    constructor(){
        //la ejecución se ejecuta aqui
        super();

        //inicializamos vacio
        this.movies = [];
        this.getMovieData();
    }

    render (){
        return html`
        ${this.movies.map(
            movie => html` <div> La pelicula ${movie.title}, fue dirigida por ${movie.director} </div>`
        )}
    `; 
    }

    getMovieData(){
        console.log("estoy en getMovieData");
        console.log("Obteniendo datos de las peliculas");

        //Variable en js -- let , var
        let xhr = new XMLHttpRequest();
        
        //el onload se llama cuando acaba el open/send
        //se ejecuta cuando recibe el callback, realmente el onload=callback
        xhr.onload = () => {
            //aqui trabajamos con el resultado de la respuesta
            if(xhr.status == 200){
                //200 ok
                console.log("Peticion completada correctamente 200");

                
                console.log(JSON.parse(xhr.responseText));
                let APIResponse =JSON.parse(xhr.responseText);

                //no asumir que la API tiene un campo RESULTS de salida
                //hacer el console log para ver qué está saliendo de la API
                this.movies=APIResponse.results;

            }
        }
        
        //open no envia la peticion
        //abrimos el canal
        xhr.open("GET", "https://swapi.dev/api/films");
        //envia la peticion, si hay q enviar un body sería: xhr.send(body);
        xhr.send();

        console.log("Fin de getMovieData");

    }
}

customElements.define('test-api', TestApi);