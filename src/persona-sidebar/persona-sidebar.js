import { LitElement, html } from "lit-element";

class PersonaSidebar extends LitElement {
    
     
    static get properties(){
        return{

            peopleStats: {type: Object}

        };
    }

    constructor(){
        super();
        this.peopleStats= {};
    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <aside>
            <section>
            <div> 
                hay <span>${this.peopleStats.numberOfPeople}</span> personas
            </div>
                <div class="mt-5">
                    <button @click="${this.newPerson}"" class="w-100 btn btn-success" style="font-size: 50px"><strong>+</strong></button>
                    </>
                </div>
            </section>
        </aside>

    `; 
    }

    newPerson(){
        console.log("newPerson");
        console.log("Se va a crear una nueva persona");

        this.dispatchEvent(new CustomEvent ("new-person", {}));
        //Donde se va a recibir el evento no necesita información, por so no le envia nada {}
    }
}

customElements.define('persona-sidebar', PersonaSidebar);