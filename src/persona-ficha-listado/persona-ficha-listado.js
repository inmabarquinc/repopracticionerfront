import { LitElement, html } from "lit-element";


class PersonaFichaListado extends LitElement {
    
     
    static get properties(){
        return{
            fname:  {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object},
            profile: {type: String}
        };
    }

    constructor(){
        super();

    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="card h-100">
            <img src="${this.photo.src}" height="350" width="250"
                 alt="${this.photo.alt}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${this.fname}</h5>
                <p class="card-text">${this.profile}</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                </ul>
            </div>
            <div class="card-footer">
                <button @click="${this.deletePerson}"class="btn btn-danger col-5"><strong>X</strong></button>
                <button @click="${this.moreInfo}" class="btn btn-info offset-1 col-5"><strong>Info</strong></button>
            </div>
        </div>
    `; 
    }

    deletePerson(){
        console.log("entro en deletePerson en persona-ficha-persona");
        console.log("entro en deletePerson en persona-ficha-persona" +this.fname);
        //se está lanzado el evento
        this. dispatchEvent(
                new CustomEvent(
                    "delete-person",
                    {
                        "detail" : {
                            name: this.fname
                        }
                    }
                )

        )
    }

    moreInfo(e){
        console.log("entro en moreInfo en persona-ficha-persona");
        console.log("Se ha pedido mas info de la persona: " +this.fname);
        //se está lanzado el evento
        this. dispatchEvent(
                new CustomEvent(
                    "info-person",
                    {
                        detail : {
                            name: this.fname
                        }
                    }
                )
        )
    }

   
}

customElements.define('persona-ficha-listado', PersonaFichaListado); 