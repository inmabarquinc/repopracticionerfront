import { LitElement, html } from "lit-element";

class PersonaStats extends LitElement {
    
     
    static get properties(){
        return{
            people: {type: Array}

        };
    }

    constructor(){
        super();

        this.people=[];

    }

    updated(changedProperties){
        //ciclo de vida de litelement
        console.log("updated en persona-stats");
        console.log(changedProperties);
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-stats");

            let peopleStats= this.gathePeopleArrayInfo(this.people);
            console.log(peopleStats);

            this.dispatchEvent(
                new CustomEvent (
                    "updated-people-stats", 
                     {
                        detail :{
                            peopleStats : peopleStats
                        }
                     }
                )
            );
        }
    }

    gathePeopleArrayInfo(people){
        console.log("gathePeopleArrayInfo en persona-stats");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);