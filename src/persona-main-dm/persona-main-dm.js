import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement {
    
     
    static get properties(){
        return{
             //se añade el array con lso datos de entrada
            //lo añadimos como propiedad
            people: {type: Array}

        };
    }

    constructor(){
        super();

         //el array se inicaliza en el constructor
         this.people =[
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley"
                },
                profile: "Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet."
            },{
                name: "Bruce Banner",
                yearsInCompany: 15,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit.Lorem ipsum dolor sit.Lorem ipsum dolor sit.Lorem ipsum dolor sit.Lorem ipsum dolor sit.Lorem ipsum dolor sit."
            },{
                name: "Eowyn",
                yearsInCompany: 20,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn"
                },
                profile: "Lorem ipsum dolor.Lorem ipsum dolor.Lorem ipsum dolor.Lorem ipsum dolor."    
            },{
                name: "Turanga Leela",
                yearsInCompany: 9,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Turanga Leela"
                },
                profile: "Lorem ipsum.Lorem ipsum."
            },{
                name: "Tyrion Lannister",
                yearsInCompany: 20,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "nomTyrion LannisterbreTres"
                },
                profile: "Lorem.Lorem. "
            }
        ]
    }

    //ciclo de vida de litelement
    updated(changedProperties){
        console.log("updated");
        if (changedProperties.has("people")){
            console.log("ha cambiado el valor de la propiedad people");

            this.dispatchEvent(
                new CustomEvent(
                        "people-data-updated",
                {
                detail:{
                     
                            people: this.people
                       
                        
                    }
                }
    
                )  
            )
        }

    }

}

customElements.define('persona-main-dm', PersonaMainDM);